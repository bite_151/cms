<?php
/**
 * Dependency injection class
 */
namespace Engine\DI;

class DI
{
    /**
     * @var array
     */
    private $container = []; // массив с зависимостями

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function set($key, $value) // сеттер зависимостей
    {
        $this->container[$key] = $value;
        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->has($key);
    }

    /**
     * @param $key
     * @return bool
     */
    public function has($key) // проверка на существование зависимости в массиве $container
    {
        return isset($this->container[$key]) ? $this->container[$key] : null ;
    }
}